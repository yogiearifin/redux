import React, {useState} from "react";

const AddTodo = ({addTodo}) => { 
    const [title, setTitle] = useState ("")

    const onSubmit = e => {
    e.preventDefault()
    const newTodo = {
        id:3,
        title:title,
        completed: false
    }
    addTodo(newTodo)
    }

    const onChange = e => {
        console.log("change")
        setTitle(e.target.value)
    }
        return(
            <div>
                <form onSubmit={onSubmit}>
                    <input value={title} onChange={onChange}></input>
                    <button>Submit</button>
                </form>
            </div>
        )
}

export default AddTodo;