import React from "react";
import "../styles/ToDoList.css"

class TodoList extends React.Component {
    render (){
    return(
        <div>
            <h1>{this.props.children}</h1>
            <p>{this.props.test}</p>
        {this.props.todos.map(item=>{
            return(
                <div key={item.id}>
                <h1>{item.id}</h1>
                <h1 className={item.completed ? "merah" : "biru"}>{item.title}</h1>
                </div>
            )
        })}
        </div>
    )
}
}

export default TodoList;