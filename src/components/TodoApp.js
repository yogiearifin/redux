import React from "react";
import TodoList from "./TodoList"
import AddTodo from "./AddTodo"

class TodoApp extends React.Component {
    state = {
        todos: [
         {
                    id: 1,
                    title: "destroy the world",
                    completed: true
         },
         {
                    id: 2,
                    title: "create a new universe",
                    completed: false
         }
               ]
    }
    addTodo = data => {
        this.setState({
            todos:[...this.state.todos,data]
        })
        console.log("ok")
    }
    render() {
        return(
            <div>
                <h1>Todo List</h1>
                <AddTodo addTodo={this.addTodo}/>
                <TodoList todos={this.state.todos} test="test doang">Test Aja</TodoList>
            </div>
        );
    }
}

export default TodoApp;